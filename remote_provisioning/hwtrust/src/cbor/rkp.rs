//! This module provides functions for dealing with RKP-specific data structures.

pub(crate) mod csr;
mod device_info;
mod protected_data;
